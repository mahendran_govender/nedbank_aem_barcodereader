/* Date : 2016/10/29
 * Author: Mahendran Govender
 * Description: Servlet that creates txt file outputs for all files in a given folder, containing the barcode values from those files
 * */

//TODO: flatten all incoming pdf files before sending to the barcode service
//TODO: output text in the format that is expected (as per examples)



package com.nedbank.aem.barcode;
 

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.ServerException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import java.io.StringWriter;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.aemfd.docmanager.Document;
import com.adobe.fd.bcf.api.BarcodedFormsService;
import com.adobe.fd.bcf.api.CharSet;

import com.adobe.fd.cpdf.api.ConvertPdfService;
import com.adobe.fd.cpdf.api.ToImageOptionsSpec;
import com.adobe.fd.cpdf.api.enumeration.ImageConvertFormat;

import com.adobe.fd.output.api.OutputService;
import com.adobe.fd.output.api.OutputServiceException;
import com.adobe.fd.output.api.PDFOutputOptions;
import com.adobe.pdfg.result.ExportPDFResult;
import com.adobe.pdfg.service.api.GeneratePDFService;




@SlingServlet(paths="/bin/NedbankBarcodeReader", methods = "POST", metatype=true)
public class BarcodedFormsServlet extends org.apache.sling.api.servlets.SlingAllMethodsServlet {
    private static final long serialVersionUID = 2598426539166789515L;
      
    protected final Logger Log = LoggerFactory.getLogger(BarcodedFormsServlet.class);
 	
 	@Reference
 	private SlingRepository repository;
 	
 	@Reference 
 	private BarcodedFormsService bcfService;
 	
 	@Reference
 	private ConvertPdfService convertPDFService;
 	
 	@Reference
 	private OutputService outputService;
 	
 	@Reference 
 	private GeneratePDFService generatePDFService;
 	

 	public void bindRepository(SlingRepository repository) {
 		this.repository = repository;
 	}
 	
 	String logName = "statusLog.txt";
 	String barcodeStatLog = "barcodeReaderStats.txt";
 	
 	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServerException, IOException {
		
		
		
		try {
			String documentFolder = request.getParameter("documentFolder");
			String outFolderName = request.getParameter("outFolder");
			String stagingFolder = request.getParameter("stagingFolder");
			String output = ""; 
			
			logToFile(outFolderName,logName, "Entered the Post Method");
			
			//Initial Loop: 
			//create tiff files of all the input file
			File folder = new File(documentFolder);
			File[] directoryListing = folder.listFiles();
		  	if (directoryListing != null) 
		  	{
			    for (File documentFile : directoryListing) 
			    {
			    	if(documentFile.isFile())
			    	{
			    		logToFile(outFolderName,logName, "Creating tiff for : " + documentFile.getName());
			    		 createTiffDocument(documentFile, stagingFolder, outFolderName);
			    	}
			    }
		  	}
		  	
		  	//Second Loop:
		  	//Use the tiff files created to extract barcode information
		  	logToFile(outFolderName,logName, "Looping through files");
		  	
		  	File stgFolder = new File(stagingFolder); //on staging folder
		  	//File stgFolder = new File(documentFolder); //<--direct
			File[] stgDirectoryListing = stgFolder.listFiles();
		  	if (stgDirectoryListing != null) 
		  	{
			    for (File documentFile : stgDirectoryListing) 
			    {    	
			    	if(documentFile.isFile())
			    	{
			    		logToFile(outFolderName,logName, "Getting barcode data for : " + documentFile.getName());
				    	String barcodeData = extractBarcode(documentFile, outFolderName, outFolderName);
				    	Boolean isBarcodeRead = false;
				    	int numberOfBarcodes = 0;
				    	
				    	//did we return any barcodes? //move this code to it's own method
				    	if(barcodeData != null)
				    	{
					    	if(barcodeData.contains("xb:barcode"))
					    	{
					    		isBarcodeRead = true;
					    		//count the barcodes
					    		Pattern p = Pattern.compile("xb:barcode");
					    		Matcher m = p.matcher( barcodeData );
					    		while (m.find()) {
					    			numberOfBarcodes++;
					    		}
					    	}
					    	else
					    	{
					    		try
					    		{
					    			//there is no barcode returned
						    		//get the file and save to failure folder
						    		Document origDoc = new Document(documentFile);
						    		origDoc.copyToFile(new File(outFolderName,documentFile.getName()));
						    		origDoc.dispose();
					    		}
					    		catch(Exception ex)
					    		{}
					    	}
				    	}
				    	else
				    	{
				    		try
				    		{
				    			//a null was returned instead of string data
					    		//get the file and save to failure folder
					    		Document origDoc = new Document(documentFile);
					    		origDoc.copyToFile(new File(outFolderName,documentFile.getName()));
					    		origDoc.dispose();
				    		}
				    		catch(Exception ex)
				    		{}
				    	}
				    	//log stats for this document
				    	logToFile(outFolderName,barcodeStatLog, documentFile.getName() + " | " + isBarcodeRead.toString() + " | " + numberOfBarcodes / 2);
				    	
				    	//log the barcodes returned
		    			LogBarcodeValues(barcodeData, documentFile.getName(), outFolderName, "barcodesFoundLog.txt");
				    	
				    	//decode the barcode to the format we want to save
		    			String decodedBarcodeData = returnOutputString(barcodeData, documentFile.getName(), outFolderName);
				    	
		    			logToFile(outFolderName,logName, "Creating barcode output file");
						
				    	String outPutPath = outFolderName  + documentFile.getName().toLowerCase().replace(".tiff", "").replace(".tif", "").replace(".pdf", "") + ".txt"; 
				    	File file = new File(outPutPath);
				    	file.createNewFile();
				    	
				    	logToFile(outFolderName,logName, "Writing decoded barcode data to output file");
				    	
				    	FileWriter fw = new FileWriter(file);
				    	BufferedWriter bw = new BufferedWriter(fw);
				    	bw.write(decodedBarcodeData.replace("&gt;", ">").replace("&lt;","<").replaceAll("[\n\r]", ""));
				    	bw.flush();
				    	bw.close();
						
						
						output += "Barcode Reading Servlet Response:" + "\n";
						output += "Document: " + documentFile + "\n";
						output += "Output Path: " + outPutPath + "\n";
						output += "Barcode Data: " + barcodeData + "\n";
						output +=  "\n";
		    		}
			    }
			 }
		  
			// Return the JSON formatted data
			JSONObject jsonReturnObj = new JSONObject();
			jsonReturnObj.put("output", output);
			String jsonData = jsonReturnObj.toJSONString();
			response.getWriter().write(jsonData);
		} 
		catch (Exception e) 
		{
			throw new Error(e);
		}
	}

    //returns the content of all barcodes in the document
 	//as xml
	private String extractBarcode(File originalFile, String outFolderName, String logFolder) { 
		
		logToFile(logFolder,logName, " - In extractBarcode method - ");
		String barcodeData = "";
	 
		try {
			 
			Document origDoc = new Document(originalFile);
			
			// Invoke decode operation of barcoded forms service 
			// Second parameter is set to true to decode PDF417 barcode symbology
			// Please see javadoc for details of parameters 
			
			org.w3c.dom.Document result = bcfService.decode(origDoc, // Input Document Object
			                                                    true, 
			                                                    false,  
			                                                    false,
			                                                    false,
			                                                    false,
			                                                    false,
			                                                    false,
			                                                    false,
			                                                    CharSet.UTF_8);// use UTF-8 character encoding 
		    
		    logToFile(outFolderName,logName, "Document barcode read");
		    // Now convert the decoded information to String
		    
		    logToFile(logFolder,logName, "Extracting barcode as text");
		    barcodeData = convertToString(result);
		    
		    origDoc.dispose();
		    return barcodeData;
		    
		} catch (FileNotFoundException e) {
			Log.error(e.getMessage());
			logToFile(logFolder,logName, "Error :" +  e.getMessage());
			//e.printStackTrace();
		} catch (IOException e) {
			Log.error(e.getMessage());
			logToFile(logFolder,logName, "Error :" +  e.getMessage());
			//e.printStackTrace();
		} catch (Exception e) {
			Log.error(e.getMessage());
			logToFile(logFolder,logName, "Error :" +  e.getMessage());
			//e.printStackTrace();
		}
		
		return null;
	}

	//Takes a pdf document or a tiff and creates a tiff in the outputfolder
	private void createTiffDocument(File originalFile, String outFolderName, String logFolder)
	{
		try
		{
			logToFile(logFolder,logName, " - In createTiffDocument method -");
			//create a document for the original file
			Document origDoc = new Document(originalFile);
			
    		if(originalFile.getName().toLowerCase().contains(".tif"))
    		{
    			logToFile(logFolder,logName, "This file is already a tiff, save it to staging folder");
    			//this is already a tif, create a document, save the document to disk, and return it as a list
    			List<Document> returnDoc = new ArrayList<Document>();
    			returnDoc.add(origDoc);
    			
    			//define the outputfile
    			File outPutFile = new File(outFolderName + originalFile.getName().toLowerCase().replace(".tif", "").replace(".tiff", "") +".tiff");
    			origDoc.copyToFile(outPutFile);
    		}
    		else
    		{
    			logToFile(logFolder,logName, "This file is not a tiff ");
    			//this is not a tiff, create one, and return an object containing the tiff
				ToImageOptionsSpec toImageOptions = new ToImageOptionsSpec();
				toImageOptions.setImageConvertFormat(ImageConvertFormat.TIFF);
				
				//flatten the document
				Document flattenedDoc = flattenPDF(originalFile, outFolderName, logFolder);
				
				//if the flattening process returned a null, use the original document
				if (flattenedDoc == null)
				{
					flattenedDoc = origDoc;
				}
				
				//invoke toImage to convert pdf to image
				logToFile(logFolder,logName, "Converting to TIFF ");
				List<Document> convertedDocs = convertPDFService.toImage(flattenedDoc, toImageOptions);
				
				//save the image/s to output folder
				logToFile(logFolder,logName, "Saving images to file");
				for(int i=0; i< convertedDocs.size(); i++)
				{
					Document pageImage = convertedDocs.get(i);
					if (convertedDocs.size() > 1)
					{
						File outPutFile = new File(outFolderName + originalFile.getName().toLowerCase() + i+1+".tiff");
						pageImage.copyToFile(outPutFile);
					}
					else
					{
						File outPutFile = new File(outFolderName + originalFile.getName().toLowerCase() +".tiff");
						pageImage.copyToFile(outPutFile);
					}
				}
				flattenedDoc.dispose();
    		}
    		origDoc.dispose();
		}
		catch(Exception ex)
		{
			Log.error(ex.getMessage());
			logToFile(logFolder,logName, "Error - " + ex.getMessage());
		}
	}

	// helper function to convert w3c document to string
	private String convertToString(org.w3c.dom.Document inputDoc) throws Exception {
	   TransformerFactory tf = TransformerFactory.newInstance();
	   Transformer tr = tf.newTransformer();
	   StringWriter sw = new StringWriter();
	   StreamResult sr = new StreamResult(sw);
	   tr.transform(new DOMSource(inputDoc), sr);
	   return sw.toString();
	 }
	
	//logs to a simple text file
	//path is expected to contain a trailing /
	//logName is expected to contain a .txt
	private void logToFile(String path, String logName, String logEntry)
	{
		try
		{
			String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
			String outPutPath = path  + logName; 
	    	File file = new File(outPutPath);
	    	if (!file.exists())
	    	{
	    		file.createNewFile();
	    	}
	    	
	    	FileWriter fw = new FileWriter(file,true);
	    	BufferedWriter bw = new BufferedWriter(fw);
	    	bw.newLine();
	    	bw.write(timeStamp + " | " + logEntry);
	    	bw.flush();
	    	bw.close();
		}
		catch(Exception ex)
		{
			//fail silently
		}
	}
	
	//flattens an enhanced pdf
	private Document flattenPDF(File pdfToFlatten, String outputFolder, String logFolder)
	{
		logToFile(logFolder, logName, "In flattenPDF method");
		Document doc=null;
	 
		try 
		{
			// Prepare service inputs
			String xfaName = pdfToFlatten.getName().toString();
		 
			PDFOutputOptions options = new PDFOutputOptions();   
			options.setContentRoot(pdfToFlatten.getParentFile().toString());

			
			logToFile(logFolder, logName, "XFA File Name: " + xfaName);
			logToFile(logFolder, logName, "Content Root:  " + options.getContentRoot());
			
		    doc = outputService.generatePDFOutput(xfaName, null, options);
		    //doc.copyToFile(new File(outputFolder ,pdfToFlatten.getName()));
		    logToFile(logFolder, logName, "Flat PDF Generated, returning");
		    
		    return doc;
		} 
		catch (OutputServiceException e) 
		{
			logToFile(logFolder, logName, "Error:  " + e.getMessage());
		} 
		catch (Exception e) 
		{
			logToFile(logFolder, logName, "Error:  " + e.getMessage());
		} 
		 
		return null;
		
	}

	//returns the string to save to file, including the barcode data
	private String returnOutputString(String xml, String filename, String logFolder)
	{
		try {
			logToFile(logFolder, logName, " - Entered Method : returnOutputString -");
		
			//String input = "abcabc pattern1foopattern2 abcdefg pattern1barpattern2 morestuff";
			List<String> barcodeValues = Arrays.asList( xml.replaceAll("^.*?<xb:content encoding=\"utf-8\">", "").split("</xb:content>.*?(<xb:content encoding=\"utf-8\">|$)"));
			
			
			String returnString = String.format("%s|%d|", filename,barcodeValues.size());
			if (barcodeValues.size() > 0)
			for(String barcode: barcodeValues)
			{
				returnString += barcode;
			}
						
			logToFile(logFolder, logName, "returning decoded barcodes for output to txt: " + barcodeValues.size());
			return returnString;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logToFile(logFolder, logName, "ERROR : " + e.getMessage());
		}
	    return "";
	}
	
	//Logs each barcode found to an output file
	private void LogBarcodeValues(String xml, String filename, String logFolder, String MethodLogFile)
	{
		try 
		{
			//get the original file type
			String type = "";
			if (filename.contains(".pdf"))
			{
				type = "pdf";
			}
			else
			{
				type = "tiff";
			}
			
			logToFile(logFolder, logName, " - Entered Method : LogBarcodeValues -");
			if (xml.contains("xb:barcode"))
			{
				//String input = "abcabc pattern1foopattern2 abcdefg pattern1barpattern2 morestuff";
				List<String> barcodeValues = Arrays.asList( xml.replaceAll("^.*?<xb:content encoding=\"utf-8\">", "").split("</xb:content>.*?(<xb:content encoding=\"utf-8\">|$)"));
				
				//<xb:content encoding="utf-8">
				//</xb:content>
				
				for (String barcode:barcodeValues)
				{
					String outputString = "";
					outputString = String.format("%s|%d|", filename,barcodeValues.size());
					outputString += barcode.replaceAll("[\n\r]", "").replace("&gt;", ">").replace("&lt;","<").replace("|", "!");
					outputString += "|" + type;
					logToFile(logFolder, MethodLogFile, outputString);
				}
			}
			else
			{
				//no barcode found
				String outputString = "";
				outputString = String.format("%s|%d|", filename,0);
				outputString += "null";
				outputString += "|" + type;
				logToFile(logFolder, MethodLogFile, outputString);
			}
						
		} catch (Exception e) {
			logToFile(logFolder, logName, "ERROR : " + e.getMessage());
		}
	}

}
